/* eslint camelcase: "off" */
// import { batch } from 'react-redux'

import { Actions, CstTekst } from '../Cst'
// import { } from '../Api/Api'

const { Foutmeldingen, OnbekendeFout } = CstTekst

export const DummyActie = () => (
  (dispatch, getState) => {
    // toon foutmelding direct
    dispatch({
      type: Actions.ZetFoutMelding,
      Foutmelding: OnbekendeFout,
    })
    // wacht even voor detail als simulatie async
    setTimeout(() => {
      // enkel detail tonen als foutmelding getoond wordt
      // als demonstratie voor up-to-date state gebruiken, ook later in de action
      const { Fouten: { IsFout } } = getState()
      if (IsFout) {
        dispatch({
          type: Actions.ZetFoutDetails,
          FoutDetails: Foutmeldingen.ApiOnbereikbaar,
        })
      }
    }, 2000)
  }
)


export const Reset = () => (
  (dispatch) => dispatch({
    type: Actions.Herstarten,
  })
)
