import { Actions } from '../Cst'

export const InitialState = {
  Fouten: {
    IsFout: false,
    Melding: null,
    Details: null,
  },
}


export const AppReducer = (state = InitialState, actie) => {
  switch (actie.type) {
    case Actions.ZetFoutMelding:
      return {
        ...state,
        Fouten: {
          IsFout: true,
          Melding: actie.Foutmelding,
          Details: state.Fouten.Details,
        },
      }

    case Actions.ZetFoutDetails:
      return {
        ...state,
        Fouten: {
          IsFout: true,
          Melding: state.Fouten.Melding,
          Details: actie.FoutDetails,
        },
      }

    case Actions.Herstarten:
      return {
        ...state, ...InitialState,
      }

    default:
      return state
  }
}
