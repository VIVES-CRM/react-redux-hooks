/* eslint {max-len:off} */
export const CstFoutAPIOnbereikbaar = 'Network Error'
export const CstApi = {
  ProdUrlBase: 'https://XXXXXXXXX.azurewebsites.net/', // Azure Functions
  DevUrlBase: 'http://localhost:7071/', // Azure Functions, lokaal debug

  UrlAPIBase: 'api/', // Azure Functions

  Fout: 'Fout bij api',
}

export const CstRoutes = {
  basename: '', // basename moet een leading slash hebben
  siteName: '/',
}

export const CstTekst = {
  Foutmeldingen: {
    ApiOnbereikbaar: 'Er kunnen geen gegevens opgehaald worden. Controleer de internetverbinding.',
  },
  LandingScherm: {
    Titel: 'React-Redux met hooks',
    Uitleg: 'Manier om redux met useSelector en useDispatch hooks te gebruiken ',
    Main: 'Welkom Wereld',
  },
  NietGevonden: {
    Tekst: 'De pagina is niet gevonden',
  },
  OnbekendeFout: 'Onbekende fout',
}

export const Actions = {
  ZetFoutMelding: 'FOUT_MELDING',
  ZetFoutDetails: 'FOUT_DETAILS',
  Herstarten: 'HERSTARTEN',

}
