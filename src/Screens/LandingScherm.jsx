import React from 'react'
import { useSelector, useDispatch } from 'react-redux'


import { CstTekst } from '../Cst'
import { DummyActie, Reset } from '../Redux/ActionCreator'


const { LandingScherm: LandingTxt } = CstTekst


const LandingScherm = () => {
  const dispatch = useDispatch()
  const Fouten = useSelector((state) => state.Fouten)
  const { IsFout, Melding, Details } = Fouten


  return (
    <React.Fragment>
      <header>
        <h1>{LandingTxt.Titel}</h1>
        <hr />
        <h3>{LandingTxt.Uitleg}</h3>

        {IsFout && (
          <div className="foutmelding">
            <h2>{`FOUT: ${Melding}`}</h2>
            <i>{Details}</i>
            <br />
          </div>
        )}

      </header>
      <main>
        <p className="titel">{LandingTxt.Main}</p>
        <button
          type="button"
          onClick={() => dispatch(DummyActie())}
        >
          Simuleer fout
        </button>

        <button
          type="button"
          onClick={() => dispatch(Reset())}
        >
          Reset
        </button>
      </main>
    </React.Fragment>
  )
}

export default LandingScherm
