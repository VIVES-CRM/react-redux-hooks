import React from 'react'
import { Switch, Route } from 'react-router-dom'
// import { CstRoutes } from '../Cst'


/* IMPORT SCREENS */
import LandingScherm from '../Screens/LandingScherm'
import NietGevonden from '../Screens/NietGevonden'

const AppRouter = () => (
  <Switch>
    <Route path="/" exact component={LandingScherm} />

    <Route component={NietGevonden} />
  </Switch>
)

export default AppRouter
